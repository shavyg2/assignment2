package com.shavauhn.musicfind;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.app.Activity;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.View;
import android.widget.EditText;

import com.shavuahn.musicfind.ArtistList;
import com.shavuahn.musicfind.R;

import external.Artist;
import external.ArtistCourier;
import external.JSONParser;


public class Main extends Activity {

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.main, menu);
		return true;
	}
	
	public void Search(View v){
		EditText t= (EditText)findViewById(R.id.editText1);
		String search=t.getText().toString();
		search=search.replace(" ", "+");
		loadWebPage(search);
	}
	
	
	
	public void loadWebPage(String search){
		String url="http://shavauhn.com/deletable/music.php?search="+search;
		Query q= new Query();
		q.execute(url);
	}
	
	
	
	class Query extends AsyncTask<String, String, JSONArray>{

		@Override
		protected JSONArray doInBackground(String... url) {
			JSONParser jp= new JSONParser();
			JSONObject jo=jp.getJSONFromUrl(url[0]);
			JSONArray ja=null;
			try {
				ja= jo.getJSONArray("response");
				
			} catch (JSONException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}	
			
			if(ja==null){
				return null;
			}
			return ja;
		}

		@Override
		protected void onPostExecute(JSONArray result) {
			super.onPostExecute(result);
			Log.i("SHAVAUHN", result.toString());
			int size=result.length();
			
			Artist[] artist_list= new Artist[size];
			for(int i=0;i<size;i++){
				artist_list[i]=new Artist();
			}
			
			for(int i=0;i<size;i++){
				try {
					JSONObject jo=result.getJSONObject(i);
					artist_list[i].fillJSON(jo);
					
				} catch (JSONException e) {
					e.printStackTrace();
					return;
				}
			}
			Intent i= new Intent(Main.this,ArtistList.class);
			ArtistCourier artist_package= new ArtistCourier(artist_list);
			i.putExtra("ArtistPackage", artist_package);
			startActivity(i);
		}
	}
}
