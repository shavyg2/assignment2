package com.shavuahn.musicfind;

import java.util.ArrayList;

import external.Artist;
import external.ArtistCourier;
import android.os.Bundle;
import android.app.Activity;
import android.content.Intent;
import android.util.Log;
import android.view.Menu;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.TextView;

public class ArtistDisplay extends Activity {

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_artist_display);
		populate();
		
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.artist_display, menu);
		return true;
	}
	
	
	public void populate(){
		Intent i= getIntent();
		Bundle extra = i.getExtras();
		Artist artist=(Artist)extra.get("artist");
		fillInfo(artist);
	}
	
	
	public void fillInfo(Artist artist){
		//TextView score=findViewById(R.id.s);
		TextView name= (TextView)findViewById(R.id.name);
		//TextView sort_name=(TextView)findViewById(R.id.sort_name);
		TextView type=(TextView)findViewById(R.id.type);
		TextView gender=(TextView)findViewById(R.id.gender);
		TextView area=(TextView)findViewById(R.id.area);
		TextView begin=(TextView)findViewById(R.id.begin);
		TextView begin_area=(TextView)findViewById(R.id.begin_area);
		TextView end=(TextView)findViewById(R.id.end);
		//TextView end_area=(TextView)findViewById(R.id.end_area);
		
		name.setText(artist.name);
		type.setText(artist.type);
		gender.setText(artist.gender);
		area.setText(artist.area);
		begin.setText(artist.begin);
		begin_area.setText(artist.begin_area);
		end.setText(artist.end);
	}

}
