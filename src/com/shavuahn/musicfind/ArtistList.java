package com.shavuahn.musicfind;


import java.util.ArrayList;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import external.Artist;
import external.ArtistCourier;

public class ArtistList extends Activity {

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_artist_list);
		populate();
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.artist_list, menu);
		return true;
	}
	
	
	public void populate(){
		Intent i= getIntent();
		Bundle extra = i.getExtras();
		ArtistCourier artist_package=(ArtistCourier)extra.get("ArtistPackage");
		
		ArrayList<Artist>list = artist_package.getAdapter();
		final ArrayList<Artist> li=list;
		
		ListView lv= (ListView)findViewById(R.id.listView1);
		
		
		
		ArrayAdapter aa= new ArrayAdapter(this, android.R.layout.test_list_item,li);
		
		lv.setAdapter(aa);
		
		
		lv.setOnItemClickListener(new OnItemClickListener() {

			@Override
			public void onItemClick(AdapterView<?> adapter, View view, int position,
					long arg3) {
					Artist artist=(Artist) adapter.getItemAtPosition(position);
					Intent i= new Intent(ArtistList.this,ArtistDisplay.class);
					i.putExtra("artist", artist);
					startActivity(i);
			}
		});
	}

}
