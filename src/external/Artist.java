package external;

import java.io.Serializable;

import org.json.JSONException;
import org.json.JSONObject;

public class Artist implements Serializable{

	public int score;
	public String name;
	public String sort_name;
	public String type;
	public String gender;
	public String area;
	public String begin;
	public String begin_area;
	public String end;
	public String end_area;

	public void fillJSON(JSONObject j){
		try {
			this.score=j.getInt("Score");
			this.name =j.getString("Name");
			this.sort_name =j.getString("Sort Name");
			this.type =j.getString("Type");
			this.gender =j.getString("Gender");
			this.area =j.getString("Area");
			this.begin =j.getString("Begin");
			this.begin_area =j.getString("Begin Area");
			this.end =j.getString("End");
			this.end_area =j.getString("End Area");
			
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
			
					
	}
	
	@Override
	public String toString(){
		return this.name;
		//return this.name.replaceAll("&quot;", "\"");
	}
}
