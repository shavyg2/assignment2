package external;

import java.io.Serializable;
import java.util.ArrayList;

public class ArtistCourier implements Serializable{
	
	Artist[] artist_list=null;
	public ArtistCourier(Artist[] artist_list) {
		this.artist_list=artist_list;
	}
	
	
	public Artist[] getPackage(){
		return this.artist_list;
	}
	
	public ArrayList<Artist> getAdapter(){
		ArrayList<Artist> al=new ArrayList<Artist>();
		Artist[] a= this.getPackage();
		int size= a.length;
		for(int i=0;i<size;i++){
			al.add(a[i]);
		}
		
		return al;
	}

}
